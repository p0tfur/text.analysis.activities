## Text Analysis Activities:
Text Analysis (or Text Mining) is the automated process of obtaining information from text, the goal is to create structured data out of free text content, which can be easier interpreted by computer. It can be used for categorizing press articles, user reviews, incoming e-mails, tickets, monitoring comments or social media and many many more.

First release contains several basic, but powerful methods and techniques:

*  Detect Language, allows text to be classified according its language. It is helpful for setting OCR engine or allows you to route e-mail or document to specific people, depending from language detected. Supports 16 languages.
*  Find Collocations, collocation helps identify words that commonly co-occur. Returns bigrams - most commonly two adjacent words i.e. "invoice date" for invoice document.
*  Prepare String To Analyze, basic activity for text analyze, prepares provided string lowercasing it, removing special characters, numbers, single letters. Returned text conatins only words separated by spaces.
*  Remove Stopwords, to provide a more accurate automated analysis of the text, it is important that we remove from play all the words that are very frequent, but provide very little semantic information or no meaning at all. These words are also known as stopwords. Supports 16 languages.
*  Word Frequency Analyse, finds most frequently used words in document, this is useful to identify document type through comparing results with defined pattern.
*  Word Position Analyse, position of word in document tells a lot about document type. "Invoice" keyword position at the top of the document - in header - gives higher possibility that this is an invoice, than if keyword is mentioned somewhere on 5th page of document.

#### Supported Languages for Detect Language and Remove Stopwords activities
* Bulgarian (bul),
* Croatian (hrv),
* Czech (ces),
* Dutch (nld),
* English (eng),
* French (fra),
* German (deu),
* Hungarian (hun),
* Italian (ita),
* Polish (pol),
* Romanian (ron),
* Russian (rus),
* Slovak (slk),
* Slovenian (slv),
* Spanish (spa),
* Turkish (tur).